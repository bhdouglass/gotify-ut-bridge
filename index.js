const WebSocket = require('ws');
const axios = require('axios');
const get = require('lodash.get');

const GOTIFY_URL = process.env.GOTIFY_URL;
const GOTIFY_KEY = process.env.GOTIFY_KEY;
const UT_PUSH_TOKEN = process.env.UT_PUSH_TOKEN; // TODO support multiple tokens for multiple devices
const UT_PUSH_URL = process.env.UT_PUSH_URL || 'https://push.ubports.com/notify';
const UT_APP_NAME = process.env.UT_APP_NAME || 'gotify.bhdouglass_gotify';

if (!GOTIFY_URL) {
    console.log('Missing GOTIFY_URL env var');
    process.exit(1);
}

if (!GOTIFY_KEY) {
    console.log('Missing GOTIFY_KEY env var');
    process.exit(1);
}

if (!UT_PUSH_TOKEN) {
    console.log('Missing UT_PUSH_TOKEN env var');
    process.exit(1);
}

const ws = new WebSocket(GOTIFY_URL, {headers: {'X-Gotify-Key': GOTIFY_KEY}});

ws.on('open', () => {
    console.log('connection opened');
});

ws.on('closed', () => {
    console.log('connection closed');
    process.exit(0);
});

ws.on('error', (err) => {
    console.log('error', err);
    process.exit(1);
});

ws.on('message', (message) => {
    let data = JSON.parse(message);
    console.log('incoming message', data);

    const extras = data.extras || {};

    // TODO maybe make this an id so notifications are sent to other devices, but not the originating device
    if (get(extras, 'ubuntu-touch::ignore', false)) {
        console.log('Ignoring message as requested');
    }
    else {
        let expire = new Date();
        expire.setUTCMinutes(expire.getUTCMinutes() + 10);

        // Push notification docs: http://docs.ubports.com/en/latest/appdev/guides/pushnotifications.html
        axios.post(UT_PUSH_URL, {
            appid: UT_APP_NAME,
            expire_on: expire.toISOString(),
            token: UT_PUSH_TOKEN,
            data: {
                notification: {
                    card: {
                        icon: get(extras, 'ubuntu-touch::icon', 'notification'),
                        summary: data.title,
                        body: data.message,
                        popup: get(extras, 'ubuntu-touch::popup', true),
                        persist: get(extras, 'ubuntu-touch::persist', true),
                        // TODO find out why this doens't work
                        //actions: [get(extras, 'ubuntu-touch::action', `application:///${UT_APP_NAME}.desktop`)],
                    },
                    vibrate: get(extras, 'ubuntu-touch::vibrate', true),
                    sound: get(extras, 'ubuntu-touch::sound', true),
                },
            },
        }).then((res) => {
            console.log('response from UT', res.status);
        }).catch((err) => {
            console.log('error sending to UT', err);
        });
    }
});
